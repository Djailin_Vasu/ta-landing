$(function () {
    var winW = $(window).outerWidth();

    if (winW <= 500) {
        // $('.ta-photos_slide  img').css({'max-width':  winW});
    }

    var winH = $(window).outerHeight();
    $('.ta-reviews_modal-content').css({
        maxHeight: (winH - 250) + 'px'
    });

    $('.ta-photos_slide').slick({
        dots: true,
        slidesToScroll: 3,
        slidesToShow: 3,
        centerMode: true,
        variableWidth: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        swipeToSlide: true,
        responsive: [{
            breakpoint: 500,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                adaptiveHeight: true,
                variableWidth: false,
                autoplay: false,
            }
        }]
    });

    $('#reviews-slider').slick({
        dots: true,
        slidesToScroll: 2,
        slidesToShow: 2,
        arrows: false,
        autoplay: false,
        swipeToSlide: true,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 1
            }
        }]
    });


    $('.ta-reviews_modal-content').mCustomScrollbar({
        theme:"dark-3",
        scrollButtons:{ enable: true }
    });



    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox({alwaysShowClose: true});
    });

    $(document).on('click', '.ta-reviews_item-text .link a', function(event) {
        event.preventDefault();
        var fam = $(this).attr('href');
        $(fam).tab('show');
        var winH = $(window).outerHeight();
        $('.ta-reviews_modal-content').css({
            maxHeight: (winH - 250) + 'px'
        });
        $('#reviewsModal').modal('show');
    });

    $(document).on('submit', 'form.ta-form', function(event) {
        event.preventDefault();
        var form = this;
        if(form.checkValidity() === false) {
            $(form).find('input, textarea, select').each(function (i, el) {
               if(el.checkValidity() === false) {
                   $(el).addClass('is-invalid');
               }
            })
        } else {
            $('#goodSubmit').modal('show');
        }
    });

    $('form.ta-form').find('input, textarea, select').keydown(function () {

        if(!this.checkValidity() === false && $(this).hasClass('is-invalid')) {
            $(this).removeClass('is-invalid');
        }
    })

    $('.ta-scroll-link').click(function (event) {
        event.preventDefault();
        var $block = $($(this).attr('href'));
        $('html, body').animate({scrollTop: $block.offset().top},500)
    });
    $('.ta-top').click(function (event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0},500)
    });

});

$(window).resize(function () {
    var winW = $(window).outerWidth();
    if (winW <= 500) {
        // $('.ta-photos_slide  img').css({'max-width':  winW});
    }

    var winH = $(window).outerHeight();
    $('.ta-reviews_modal-content').css({
        maxHeight: (winH - 250) + 'px'
    });
});

$(window).scroll(function () {
    if($(window).scrollTop() >= $('#first-slide').outerHeight()/3) {
        $('.ta-top').addClass('active');
    } else {
        $('.ta-top').removeClass('active');
    }
});



